<html>
	<head>
		<title>Ejemplo de operadores de Comparacion</title>
	</head>
	<body>
		<h1>Ejemplo de operaciones comparacion en PHP</h1>
		<?php
			$a = 8;
			$b = 3;
			$c = 3;
			echo $a == $b, "<br>";
			echo $a != $b, "<br>";
			echo $a < $b, "<br>";
			echo $a > $b, "<br>";
			echo $a >= $c, "<br>";
			echo $a <= $c, "<br>";			
/*

Anote el significado de las operaciones de comparacion:
Operador == "igual"
Operador != "No sea igual"
Operador < "Menor que"
Operador > "Mayor que"
Operador >= "Mayor o igual que"
Operador <= "Menor o igual que"
			
*/
		?>
	</body>
</html>
